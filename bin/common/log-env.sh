#!/bin/bash

##
## Log Environment
##

echo ""
echo "Environment:"
echo ""
echo "Service      : ${APP_NAME_RAW}"
echo "Stage        : ${STAGE}"
echo "App [Staged] : ${APP_NAME_FINAL}"
echo "App [Smoke]  : ${APP_NAME_SMOKE}"
echo "Host [Staged]: ${HOST_NAME_FINAL}"
echo "Host [Smoke] : ${HOST_NAME_SMOKE}"
echo "Domain       : ${DOMAIN}"
echo "Path         : ${URL_PATH}"
echo ""
