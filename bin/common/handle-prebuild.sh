#!/bin/bash

##
## Handle Prebuild
##

if [ -n "$PREBUILD" ]; then

  echo "Using pre-built swift code..."
  
  # Delete a line with buildpack prefix
  sed -i '/  buildpack:/d' ./manifest.yml
  
  # Add a line with buildpack
  if grep -q cflinuxfs3 ./manifest.yml; then
    echo "  buildpack: https://github.com/fuzed-innovations/swift-buildpack#pre-build-cflinuxfs3" >> ./manifest.yml
  else 
    echo "  buildpack: https://github.com/fuzed-innovations/swift-buildpack#pre-build" >> ./manifest.yml
  fi
  
  cat ./manifest.yml
  
else
  echo "Not pre-building..."
fi
