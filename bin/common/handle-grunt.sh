#!/bin/bash

##
## RUN GRUNT PRE-PROCESSOR
##

if [ -f "./Gruntfile.js" ] && [ -f "./package.json" ] && [ -n "$GRUNTBUILD" ]; then
    echo "Running grunt prebuilder."
    export PATH=/opt/IBM/node-v6.7.0/bin:$PATH
    npm install
    grunt
else
    echo "Not using grunt prebuilder."
fi
