#!/bin/bash

##
## Main Pipeline
##

SCRIPT_PATH="$(cd "$(dirname "$BASH_SOURCE")"; pwd)"

if [ -z "$SWIFTY_MICROSERVICE_VERSION" ]; then
    echo "Environment variable 'SWIFTY_MICROSERVICE_VERSION' must be set"
    exit 1
fi

if [ -z "$STAGE" ]; then
    echo "Environment variable 'STAGE' must be set"
    exit 1
fi

. ${SCRIPT_PATH}/common/setup-ssh.sh
. ${SCRIPT_PATH}/common/load-env.sh
. ${SCRIPT_PATH}/common/handle-grunt.sh
. ${SCRIPT_PATH}/common/handle-prebuild.sh
. ${SCRIPT_PATH}/swifty-microservice-${SWIFTY_MICROSERVICE_VERSION}/load-env.sh
. ${SCRIPT_PATH}/common/log-env.sh
. ${SCRIPT_PATH}/common/deploy-smoke.sh
. ${SCRIPT_PATH}/common/blue-green.sh
