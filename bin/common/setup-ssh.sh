#!/bin/bash

##
## SSH SETUP
##

### Required Environment
###
### SSH_PRIV_KEY_BASE64 : Base 64 Encoded SSH Private Key with Git Repo Access
###

echo "Setting up SSH"

if [ -z "$SSH_PRIV_KEY_BASE64" ]; then
    echo "Environment variable 'SSH_PRIV_KEY_BASE64' must be set"
    exit 1
fi

mkdir .ssh
touch .ssh/known_hosts

# make sure SSH accepts the host key
echo "Running ssh-keyscan"
ssh-keyscan -H github.com >> .ssh/known_hosts
ssh-keyscan -H gitlab.com >> .ssh/known_hosts

# set the SSH private key
echo "Loading key from ENV"
echo "$SSH_PRIV_KEY_BASE64" | base64 -d > .ssh/id_rsa
chmod 600 .ssh/id_rsa

# configure SSH to use the private key for connections
echo "Setting up .ssh/config"
touch .ssh/config
cat > .ssh/config << EOF
host github.com
HostName github.com
IdentityFile ~/.ssh/id_rsa
User git

host gitlab.com
HostName gitlab.com
IdentityFile ~/.ssh/id_rsa
User git
EOF

