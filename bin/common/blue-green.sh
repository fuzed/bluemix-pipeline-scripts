#!/bin/bash

##
## DEPLOY FOR BLUE_GREEN
##

if [ $CURRENT_INSTANCES == 0 ]
then

    # First Time Deploy
    cf rename ${APP_NAME_SMOKE} ${APP_NAME_FINAL}

else

    # Repeat Deploy

    # Rename old app to have -secondary suffix

    echo "Renaming old and new apps to reference current load balancing state"

    APP_NAME_PRIMARY=${APP_NAME_FINAL}-primary
    APP_NAME_SECONDARY=${APP_NAME_FINAL}-secondary

    cf rename ${APP_NAME_SMOKE} ${APP_NAME_PRIMARY}
    cf rename ${APP_NAME_FINAL} ${APP_NAME_SECONDARY}

    # Perform tests against production routes, using ${APP_NAME_PRIMARY}
    # TODO: Test

    # Power off the old version, leaving routes still bound for rapid recovery

    printf "\nStopping ${APP_NAME_SECONDARY} (old)\n"
    cf stop ${APP_NAME_SECONDARY}


    # Give legacy version a timestamped name

    printf "\nRenaming old and new apps to reference live/backup state\n"

    CURRENT_TIMESTAMP="$(date +%s)"
    APP_NAME_BACKUP=${APP_NAME_FINAL}-backup-${CURRENT_TIMESTAMP}

    cf rename ${APP_NAME_SECONDARY} ${APP_NAME_BACKUP}
    cf rename ${APP_NAME_PRIMARY} ${APP_NAME_FINAL}

    printf "\nThe old instance now exists as ${APP_NAME_BACKUP}"
    printf "\nRestart the instance to rollback."

fi