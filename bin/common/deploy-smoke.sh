#!/bin/bash

##
## INSTALL DEPENDENCIES
##

echo "Installing JQ"

curl -o jq -L "https://github.com/stedolan/jq/releases/download/jq-1.5/jq-linux64"
chmod +x jq
export PATH=$PATH:$PWD

##
## HELPER FUNCTIONS
##

appInstanceCount() {
    # TODO: Check how many instances of $1 are running
    NUM_INSTANCES=$( cf curl /v2/apps?q=name%20IN%20${1} | jq -r .resources[0].entity.instances )
    NUM_INSTANCES=$(($NUM_INSTANCES + 0))
    echo $NUM_INSTANCES
}

maxnum() {
    if [ $2 -gt $1 ]
    then
        echo $2
    else
        echo $1
    fi
}

##
## DEPLOY FOR SMOKE TESTS
##

# Deploy one app instance ready for smoke testing

echo "Deploying for smoke tests"

cf push ${APP_NAME_SMOKE} -i 1 --no-route

# Map to smoke testing routes

echo "Mapping route ready for smoke tests"

cf map-route ${APP_NAME_SMOKE} ${DOMAIN} --hostname "${HOST_NAME_SMOKE}" --path "${URL_PATH}"

# Determine how many instances are required

DEFAULT_INSTANCES=1
CURRENT_INSTANCES=$( appInstanceCount ${APP_NAME_FINAL} )

echo "Found ${CURRENT_INSTANCES} instances of ${APP_NAME_FINAL}"

DESIRED_INSTANCES=$( maxnum ${DEFAULT_INSTANCES} ${CURRENT_INSTANCES} )

cf scale ${APP_NAME_SMOKE} -i ${DESIRED_INSTANCES}

# Switch new app to final routes

echo "Switching ${APP_NAME_SMOKE} to final route"

cf map-route ${APP_NAME_SMOKE} ${DOMAIN} --hostname "${HOST_NAME_FINAL}" --path "${URL_PATH}"
cf unmap-route ${APP_NAME_SMOKE} ${DOMAIN} --hostname "${HOST_NAME_SMOKE}" --path "${URL_PATH}"
