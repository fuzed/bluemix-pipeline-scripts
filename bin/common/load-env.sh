#!/bin/bash

##
## Load Environment
##

### Required Environment
###
### STAGE : A string representing the stage of deployment
###

if [ -z "$STAGE" ]; then
    echo "Environment variable 'STAGE' must be set"
    exit 1
fi

### Required Configuration
###
### manifest.yml :
### > Cloudfoundry Manifest with at least a name defined
###

# Install YML Processing Dependency

echo "Installing shyaml"
sudo pip install git+https://github.com/0k/shyaml
echo "Installed shyaml"

# Extract app name from manifest.yml
# TODO: Fallback / override using pipeline specified value ${CF_APP}

echo "Loading environment from manifest.yml"

APP_NAME_RAW=$(cat manifest.yml | shyaml get-value applications.0.name '')
if [ -z "$APP_NAME_RAW" ]; then
    echo "An application name must be specified in the manifest.yml"
    exit 1
fi

APP_NAME_FINAL=${APP_NAME_RAW}-${STAGE}
APP_NAME_SMOKE=${APP_NAME_FINAL}-smoke
