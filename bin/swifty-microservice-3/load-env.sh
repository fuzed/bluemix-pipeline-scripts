#!/bin/bash

##
## Load Environment - SwiftyMicroservice 3
##

HOST_NAME_FINAL=${APP_NAME_RAW}
HOST_NAME_SMOKE=${HOST_NAME_FINAL}-smoke

if [ "${STAGE}" == "prod" ]; then
    DOMAIN="msa.fuzed.io"
else
    DOMAIN="msa.${STAGE}.fuzed.io"
fi

URL_PATH=""
